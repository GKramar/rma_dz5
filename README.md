# Gdje je Gabrijela? #

## Opis aplikacije ##

Aplikacija *Gdje je Gabrijela?* prikazuje trenutnu lokaciju mobilnog uređaja uz pomoć Google karti i ispisuje geografsku širinu, visinu, državu, mjesto i adresu trenutne lokacije. Na karti je moguće dodati i/ili ukloniti marker prema želji, gdje pri dodavanju markera aplikacija pušta zvuk. Dodatne mogućnosti su snimanje karte klikom na gumb *SNIMI KARTU* i snimanje fotografije pomoću kamere mobitela klikom na gumb *SNIMI FOTOGRAFIJU*. Kada se slika snimi, sprema se na uređaj i pojavljuje se notifikacija. Klikom na notifikaciju, otvara se uslikana slika.

## Rješenja i problemi pri stvaranju aplikacije ##

Dodavanje Google karte, markera klikom na kartu i dopuštenja napravljeni su prema predlošku za LV5. Prvi problem se za *DexIndexOverflowException* gdje je rješenje nađeno pod [1] i omogućen *multidex support*. Praćenje trenutne lokacije preko Google karte napravljeno je uz *onMyLocationChange*, a primjer je nađen na [2]. Za stalno prikazivanje opisa markera za trenutnu lokaciju korištena je metoda koja je nađena pod [3]. Pri dodavanju markera, fokus se pomiče na dodani marker ([4]) i pušta se zvuk preko *SoundPool-a* koji je napravljen prema predlošku za LV5, a zvuk je preuzet s [11]. S obzirom da se markeri mogu dodavati na kartu, logično je da bi se trebali nekako i brisati pa je nađena metoda *onMarkerClick* na [5] i *remove()* na [6]. Slikanje pomoću kamere je napravljeno uz pokretanje *Activitya* radi rezultata i link [7]. Za spremanje slike, napravljena je funkcija *saveImage* i komentara s linka [8]. Notifikacije su napravljene po uzoru na predložak za LV5, dok je omogućeno i otvaranje slike klikom na notifikaciju, a koje je rješenje nađeno na [9]. Pri testiranju je otkriveno da na uređaji kod kojih se ne može izvršiti *getExternalStorageDirectory()*, slika se ne otvara klikom na notifikaciju, no slika je spremljena u galeriji pa se može tamo vidjeti. Još je dodana mogućnost slikanja karte pomoću *onSnapshotReady* metode, a slika se dalje obrađuje kao i slika uslikana s kamerom. Isti problem se pojavljuje pri spremanju slike na uređaj, ako se na uređaju ne može dobiti spremnik pomoću *getExternalStorageDirectory()*. Na kraju je aplikacija postavljena da radi samo u *portretnom* načinu rada ([10]).
Marker za trenutnu lokaciju preuzet je s [12], a marker koji se dodaje klikom je preuzet s [13].

## Testiranje ##

Testiranje je obavljeno na dva uređaja, prvi je 5.5'' FHD mobilni uređaj (detalji na [7]) i screenshot-ovi su u mapi Screenshots_DZ4 pod nazivom test1_X, a drugi je 5'' HD mobilni uređaj (detalji na [8]) čiji su screenshot-ovi spremljeni pod nazivom test2_X.

### Linkovi na vanjske resurse ###

[1] http://stackoverflow.com/questions/38714651/android-studio-dexindexoverflowexception-method-id-not-in

[2] http://stackoverflow.com/questions/21403496/how-to-get-current-location-in-google-map-android

[3] https://developers.google.com/maps/documentation/android-api/infowindows

[4] http://stackoverflow.com/questions/14074129/google-maps-v2-set-both-my-location-and-zoom-in

[5] https://developers.google.com/android/reference/com/google/android/gms/maps/GoogleMap.OnMarkerClickListener

[6] http://stackoverflow.com/questions/13692398/remove-a-marker-from-a-googlemap

[7] http://stackoverflow.com/questions/5991319/capture-image-from-camera-and-display-in-activity

[8] http://stackoverflow.com/questions/32624133/how-to-take-a-screen-shot-on-a-button-click-can-anyone-provide-a-android-code

[9] http://stackoverflow.com/questions/23123767/notification-pressed-to-open-up-file-in-default-app-android

[10] http://stackoverflow.com/questions/15718869/how-to-lock-android-apps-orientation-to-portrait-mode

[11] http://soundbible.com/266-Boing-Cartoonish.html

[12] https://www.iconfinder.com/icons/101894/favorite_location_pin_star_start_icon#size=128

[13] https://www.iconfinder.com/icons/68011/blue_location_pin_icon#size=128

[14] http://www.devicespecifications.com/en/model/3a353426

[15] http://www.gsmarena.com/lenovo_c2_power-8368.php