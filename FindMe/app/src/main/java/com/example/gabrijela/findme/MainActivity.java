package com.example.gabrijela.findme;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    GoogleMap mGoogleMap;
    MapFragment mMapFragment;
    private GoogleMap.OnMapClickListener mCustomOnMapClickListener;
    private static final int REQUEST_LOCATION_PERMISSION = 17;
    TextView tvLatitude, tvLongitude, tvCountry, tvLocality, tvAddress;
    Location mLastLocation = null;
    Marker mCurrLocationMarker = null;
    LocationManager mLocationManager;
    String message, locality, state, address;
    SoundPool mSoundPool;
    boolean mLoaded = false;
    HashMap<Integer, Integer> mSoundMap = new HashMap<>();
    Button bTakeScreenshot, bTakeImage;
    String notificationTitle = "Spremljena je nova slika";
    String notificationText;
    File imageFile;
    private static final int CAMERA_REQUEST = 1254;
    private GoogleMap.OnMyLocationChangeListener mCustomLocationChangeListener;
    private GoogleMap.OnMarkerClickListener mCustomMarkerClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initialize();
        this.mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        this.loadSounds();
    }

    private void initialize() {
        this.tvLatitude = (TextView) findViewById(R.id.tvLatitude);
        this.tvLongitude = (TextView) findViewById(R.id.tvLongitude);
        this.tvCountry = (TextView) findViewById(R.id.tvCountry);
        this.tvLocality = (TextView) findViewById(R.id.tvLocality);
        this.tvAddress = (TextView) findViewById(R.id.tvAddress);
        this.bTakeScreenshot = (Button) findViewById(R.id.bTakeMapSnapshot);
        this.bTakeImage = (Button) findViewById(R.id.bTakePicture);
        bTakeImage.setOnClickListener(this);
        bTakeScreenshot.setOnClickListener(this);
        this.mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fGoogleMap);
        this.mMapFragment.getMapAsync(this);
        this.mCustomOnMapClickListener = new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions newMarkerOptions = new MarkerOptions();
                newMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker));
                newMarkerOptions.position(latLng);
                mGoogleMap.addMarker(newMarkerOptions);
                if (mCurrLocationMarker != null) mCurrLocationMarker.showInfoWindow();
                if (mLoaded) playSound(R.raw.boing_cartoonish);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
                mGoogleMap.animateCamera(cameraUpdate);
            }
        };
        this.mCustomMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.remove();
                mCurrLocationMarker.showInfoWindow();
                return true;
            }
        };
        this.mCustomLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                updateLocationDisplay(location);
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        UiSettings uiSettings = this.mGoogleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        this.mGoogleMap.setOnMapClickListener(this.mCustomOnMapClickListener);
        this.mGoogleMap.setOnMyLocationChangeListener(this.mCustomLocationChangeListener);
        this.mGoogleMap.setOnMarkerClickListener(this.mCustomMarkerClickListener);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
            return;
        }
        this.mGoogleMap.setMyLocationEnabled(true);
    }

    private void requestPermission() {
        String[] permissions = new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION};
        ActivityCompat.requestPermissions(MainActivity.this,
                permissions, REQUEST_LOCATION_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permission", "Permission granted. User pressed allow.");
                    } else {
                        Log.d("Permission", "Permission not granted. User pressed deny.");
                        askForPermission();
                    }
                }
        }
    }

    private void askForPermission() {
        boolean shouldExplain = ActivityCompat.shouldShowRequestPermissionRationale(
                MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (shouldExplain) {
            Log.d("Permission", "Permission should be explained, - don't show again not clicked.");
            this.displayDialog();
        } else {
            Log.d("Permission", "Permission not granted. User pressed deny and don't show again.");
        }
    }

    private void displayDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Location permission")
                .setMessage("We display your location and need your permission")
                .setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("Permission", "User declined and won't be asked again.");
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("Permission", "Permission requested because of the explanation.");
                        requestPermission();
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void updateLocationDisplay(Location location) {
        message = "Geografska dužina: " + location.getLongitude();
        tvLongitude.setText(message);
        message = "Geografska širina: " + location.getLatitude();
        tvLatitude.setText(message);
        if (Geocoder.isPresent()) {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> nearByAddresses = geocoder.getFromLocation(
                        location.getLatitude(), location.getLongitude(), 1);
                if (nearByAddresses.size() > 0) {
                    Address nearestAddress = nearByAddresses.get(0);
                    state = nearestAddress.getCountryName();
                    message = "Država: " + state;
                    tvCountry.setText(message);
                    locality = nearestAddress.getLocality();
                    message = "Mjesto: " + locality;
                    tvLocality.setText(message);
                    address = nearestAddress.getAddressLine(0);
                    message = "Adresa: " + address;
                    tvAddress.setText(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        MarkerOptions newMarkerOptions = new MarkerOptions();
        newMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.here_icon));
        newMarkerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
        newMarkerOptions.title("Ovdje sam");
        newMarkerOptions.snippet("Wow! Konačno znam gdje se nalazim!");
        mCurrLocationMarker = mGoogleMap.addMarker(newMarkerOptions);
        mCurrLocationMarker.showInfoWindow();
    }

    private void loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = new SoundPool.Builder().setMaxStreams(10).build();
        } else {
            this.mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }
        this.mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Log.d("Test", String.valueOf(sampleId));
                mLoaded = true;
            }
        });
        this.mSoundMap.put(R.raw.boing_cartoonish, this.mSoundPool.load(this, R.raw.boing_cartoonish, 1));
    }

    void playSound(int selectedSound) {
        int soundID = this.mSoundMap.get(selectedSound);
        this.mSoundPool.play(soundID, 1, 1, 1, 0, 1f);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.bTakeMapSnapshot):
                captureScreen();
                break;
            case (R.id.bTakePicture):
                takePicture();
                break;
        }
    }

    public void captureScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                sendNotification(saveImage(snapshot));
            }
        };
        mGoogleMap.snapshot(callback);
    }

    private void takePicture() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == MainActivity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            sendNotification(saveImage(photo));
        }
    }

    private File saveImage(Bitmap bitmap) {
        message = "/" + locality + address + ".png";
        notificationText = Environment.getExternalStorageDirectory() + message;
        imageFile = new File(Environment.getExternalStorageDirectory() + message);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("TAG", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("TAGC", e.getMessage(), e);
        }
        return imageFile;
    }

    private void sendNotification(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setAutoCancel(true)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setSmallIcon(android.R.drawable.ic_menu_camera)
                .setContentIntent(notificationPendingIntent)
                .setLights(Color.BLUE, 2000, 1000)
                .setVibrate(new long[]{1000, 1000, 1000})
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        Notification notification = notificationBuilder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
}

